/* eslint-disable no-undef */
import chai from "chai";
import request from "supertest";

const expect = chai.expect;

let api;

const sampleActor = {
	id: 90633,
	name: "Gal Gadot"
};

describe("People Endpoint", () => {
	beforeEach(async () => {
		try {
			api = require("../../../../index");
		} catch (err) {
			console.error(`failed to Load user data: ${err}`);
		}
	});
	
	afterEach(() => {
		api.close(); //Release Pport 8080
		delete require.cache[require.resolve("../../../../index")];
	});

	describe("GET /people", () => {
		it("should return the details of 20 actors and a status 200", (done) => {
			request(api)
			.get("/api/people")
			.set("Accept", "application/json")
         .expect("Content-Type", /json/)
			.expect(200)
			.end((err, res) => {
				expect(res.body).to.be.a('array');
				expect(res.body.length).to.be.equal(20);
				done();
			});
		});
	});

	describe("GET /people/:id", () => {
		describe("when the id is valid", () => {
			it("should return the matching person", () => {
				return request(api)
				.get(`/api/people/${sampleActor.id}`)
				.expect("Content-Type", /json/)
				.expect(200)
				.then((res) => {
					expect(res.body).to.have.property("name", sampleActor.name);
				});
			});
		});
	});

	describe("when the id is invalid", () => {
		it("should retuen NOT found message", () => {
			return request(api)
			.get("/api/people/xxxx")
			.set("Accept", "application/json")
			.expect("Content-Type", /json/)
			.expect({
				success: false,
				status_code: 34,
				status_message: "The resource you requested could not be found."
			});
		});
	});

	//Add a favourite Actor
	describe("POST and check favoururite Actors", () => {
		it("should add Gal Gadot as a favourite actor and check that she has been added", () => {
			return request(api)
			.post("/api/users/user1/favouriteActors")
			.send({
				"id": 90633,
				"name": "Gal Gadot"
			})
			.expect(201)
			.then(
				request(api)
				.get("/api/users/user1/favouriteActors")
				.set("Accept", "application/json")
				.expect("Content-Type", /json/)
				.expect(200)
				.then((res) => {
					expect(res.body).to.have.property("name", sampleActor.name);
				})
			);
		});
	});
});