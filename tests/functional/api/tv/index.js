/* eslint-disable no-undef */
import chai from "chai";
import request from "supertest";

const expect = chai.expect;

let api;

const sampleShow = {
   id: 82856,
   original_name: "The Mandalorian",
};

describe("Tv endpoint", () => {
   beforeEach(async () => {
      try {
         api = require("../../../../index");
      } catch (err) {
         console.error(`failed to Load user data: ${err}`);
      }
   });

   afterEach(() => {
      api.close(); //Release Port 8080
      delete require.cache[require.resolve("../../../../index")];
   });

   describe("GET /tv ", () => {
      it("should return 20 tv shows and a status 200", (done) => {
         request(api)
         .get("/api/tv")
         .set("Accept", "application/json")
         .expect("Content-Type", /json/)
         .expect(200)
         .end((err, res) => {
            expect(res.body).to.be.a('array');
            expect(res.body.length).to.equal(20);
            done();
         });
      });
   });

   describe("GET /tv/:id", () => {
      describe("when the id is valid", () => {
         it("should return the matching tv show", () => {
            return request(api)
            .get(`/api/tv/${sampleShow.id}`)
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((res) => {
               expect(res.body).to.have.property("name", sampleShow.original_name);
            });
         });
      });

      describe("when the id is invalid", () => {
         it("should return NOT found message", () => {
            return request(api)
            .get("/api/tv/xxx")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect({
              success: false,
              status_code: 34,
              status_message: "The resource you requested could not be found.",
            });
         });
      });
      
      // Add a Favourite Movie
      describe("POST and check favourite Shows", () => {
         it("should add The Mandalorian as a favourite and check that it has been added", () => {
            return request(api)
            .post("/api/users/user1/favouriteShows")
            .send({
               "id": 82856,
               "title": "The Mandalorian"
            })
            .expect(201)
            .then(
               request(api)
               .get("api/users/user1/favouriteShows")
               .set("Accept", "application/json")
               .expect("Content-Type", /json/)
               .expect(200)
               .then((res) => {
                  expect(res.body).to.have.property("title", sampleShow.original_name);
               })
            );
         });
      });
   });
});