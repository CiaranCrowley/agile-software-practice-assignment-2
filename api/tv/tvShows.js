export const tvShowsObject = {
   "tvShows": [
      {
         "name": "The Mandalorian",
         "poster_path": "/sWgBv7LV2PRoQgkxwlibdGXKz1S.jpg",
         "overview": "After the fall of the Galactic Empire, lawlessness has spread throughout the galaxy. A lone gunfighter makes his way through the outer reaches, earning his keep as a bounty hunter.",
         "first_air_date": "2019-11-12",
         "genre_ids": [
            10765,
            10759
         ],
         "id": 82856,
         "original_name": "The Mandalorian",
         "original_language": "en",
         "backdrop_path": "/o7qi2v4uWQ8bZ1tW3KI0Ztn2epk.jpg",
         "popularity": 1475.058,
         "vote_count": 4631,
         "vote_average": 8.5
      },
      {
         "name": "The Good Doctor",
         "poster_path": "/6tfT03sGp9k4c0J3dypjrI8TSAI.jpg",
         "overview": "A young surgeon with Savant syndrome is recruited into the surgical unit of a prestigious hospital. The question will arise: can a person who doesn't have the ability to relate to people actually save their lives?",
         "first_air_date": "2017-09-25",
         "genre_ids": [
            18
         ],
         "id": 71712,
         "original_name": "The Good Doctor",
         "original_language": "en",
         "backdrop_path": "/zlXPNnnUlyg6KyvvjGd2ZxG6Tnw.jpg",
         "popularity": 847.504,
         "vote_count": 6182,
         "vote_average": 8.6
      }
   ]
};