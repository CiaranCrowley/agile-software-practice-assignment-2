import express from "express";
import { getTvShows, getShow } from "../tmdb-api";

const router = express.Router();

router.get("/" , (req, res, next) => {
   getTvShows()
   .then((tv) => res.status(200).send(tv))
   .catch((error) => next(error));
});

router.get("/:id" , (req, res, next) => {
   const id = parseInt(req.params.id);
   getShow(id)
   .then((tv) => res.status(200).send(tv))
   .catch((error) => next(error));
});

export default router;