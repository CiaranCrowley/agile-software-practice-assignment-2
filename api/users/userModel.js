/* eslint-disable no-unused-vars */
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const MovieSchema = new Schema({
  id: Number,
  title: String
});

const TvShowsSchema = new Schema({
  id: Number,
  title: String
});

const PeopleSchema = new Schema({
  id: Number,
  name: String
});

const UserSchema = new Schema({
  username: { type: String, unique: true, required: true},
  password: {type: String, required: true },
  favourites: [MovieSchema],
  favouriteShows: [TvShowsSchema],
  favouriteActors: [PeopleSchema]
});

UserSchema.statics.findByUserName = function (username) {
  return this.findOne({ username: username });
};

export default mongoose.model('User', UserSchema);