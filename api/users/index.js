/* eslint-disable no-unused-vars */
import express from "express";
import User from "./userModel";

const router = express.Router(); // eslint-disable-line

// Get all users
router.get("/", (req, res) => {
	User.find().then((users) => res.status(200).json(users));
});

//Get Favoutite Movies
router.get('/:userName/favourites', (req, res, next) => {
	const userName = req.params.userName;
	User.findByUserName(userName).populate('favourites').then(
		user => res.status(201).json(user.favourites)
	).catch(next);
});

//Get Favourite TV Shows
router.get('/:userName/favouriteShows', (req, res, next) => {
	const userName = req.params.userName;
	User.findByUserName(userName).populate('favouriteShows').then(
		user => res.status(201).json(user.favouriteShows)
	).catch(next);
});

//Get Favourite Actors
router.get('/:userName/favouriteActors', (req, res, next) => {
	const userName = req.params.userName;
	User.findByUserName(userName).populate('favouroiteActors').then(
		user => res.status(201).json(user.favouriteActors)
	).catch(next);
});

// register
router.post("/", (req, res, next) => {
	User.create(req.body)
		.then((user) =>
			res.status(200).json({ success: true, token: "FakeTokenForNow" })
		)
		.catch( (error) => next(error));
});

// Update a user
router.put("/:id", (req, res) => {
	if (req.body._id) delete req.body._id;
	User.update(
		{
			_id: req.params.id,
		},
		req.body,
		{
			upsert: false,
		}
	).then((user) => res.json(200, user));
});

//Post Favourite Movies
router.post('/:userName/favourites', (req, res, next) => {
	const newFavourite = req.body;
	const query = {username: req.params.userName};
	if (newFavourite && newFavourite.id) {
		User.find(query).then(
			user => {
				(user.favourites)?user.favourites.push(newFavourite):user.favourites =[newFavourite];
				User.findOneAndUpdate(query, {favourites:user.favourites}, {
					new: true
				}).then(user => res.status(201).send(user));
			}
		).catch( (error) => next(error) );
	} else {
		res.status(401).send(`Unable to find user ${req.params.userName} `);
	}
});

//Post Favourite Tv Shows
router.post('/:userName/favouriteShows', (req, res, next) => {
	const newFavShow = req.body;
	const query = {username: req.params.userName};
	if (newFavShow && newFavShow.id) {
		User.find(query).then (
			user => {
				(user.favouriteShows)?user.favouriteShows.push(newFavShow):user.favouriteShows = [newFavShow];
				User.findOneAndUpdate(query, {favouriteShows:user.favouriteShows}, {
					new: true
				}).then(user => res.status(201).send(user));
			}
		).catch( (error) => next(error) );
	} else {
		res.status(401).send(`Unable to find user ${req.params.userName} `);
	}
});

//Post Favourite Actors
router.post('/:userName/favouriteActors', (req, res, next) => {
	const newFavActor = req.body;
	const query = {username: req.params.userName};
	if (newFavActor && newFavActor.id) {
		User.find(query).then (
			user => {
				(user.favouriteActors)?user.favouriteActors.push(newFavActor):user.favouriteActors = [newFavActor];
				User.findOneAndUpdate(query, {favouriteActors:user.favouriteActors}, {
					new: true
				}).then(user => res.status(201).send(user));
			}
		).catch( (error) => next(error) );
	} else {
		res.status(401).send(`Unable to find user ${req.params.userName} `);
	}
});

export default router;
