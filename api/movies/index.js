/* eslint-disable no-unused-vars */
import express from "express";
import { getMovies, getMovie, getMovieReviews,
	getUpcomingMovies, getPopularMovies, getLatestMovies } from "../tmdb-api";

const router = express.Router();

router.get("/", (req, res, next) => {
	getMovies()
	.then((movies) => res.status(200).send(movies))
	.catch((error) => next(error));
});

router.get("/:id", (req, res, next) => {
	const id = parseInt(req.params.id);
	getMovie(id)
	.then((movie) => res.status(200).send(movie))
	.catch((error) => next(error));
});

router.get("/:id/reviews", (req, res, next) => {
	const id = parseInt(req.params.id);
	getMovieReviews(id)
	.then((reviews) => res.status(200).send(reviews))
	.catch((error) => next(error));
});

router.get("/upcoming", (req, res, next) => {  //Upcoming returns the same results as discover movies, but TMDB returns different results
	getUpcomingMovies()
	.then((movies) => res.status(200).send(movies))
	.catch((error) => next(error));
});

router.get("/popularMovies", (req, res, next) => {  //According to TMDB, discover movies and popular movies currently return the same results
	getPopularMovies()
	.then((popular) => res.status(200).send(popular))
	.catch((error) => next(error));
});

// router.get("/latestMovies", (req, res, next) => {  //Returns the same results as discover movies but shouldne
// 	getLatestMovies()
// 	.then((latest) => res.status(200).send(latest))
// 	.catch((error) => next(error));
// });

export default router;
