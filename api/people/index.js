/* eslint-disable no-unused-vars */
import express from "express";
import { getPeople, getPerson } from "../tmdb-api";

const router = express.Router();

router.get("/", (req, res, next) => {
   getPeople()
   .then((people) => res.status(200).send(people))
   .catch((error) => next(error));
});

router.get("/:id", (req, res, next) => {
   const id = parseInt(req.params.id);
   getPerson(id)
   .then((person) => res.status(200).send(person))
   .catch((error) => next(error));
});

export default router;