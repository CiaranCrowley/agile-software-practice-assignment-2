export const peopleObject = {
   "people" : [
      {
         "gender": 1,
         "id": 90633,
         "known_for": [
            {
               "adult": false,
               "backdrop_path": "/6iUNJZymJBMXXriQyFZfLAKnjO6.jpg",
               "genre_ids": [
                  28,
                  12,
                  14
               ],
               "id": 297762,
               "media_type": "movie",
               "original_language": "en",
               "original_title": "Wonder Woman",
               "overview": "An Amazon princess comes to the world of Man in the grips of the First World War to confront the forces of evil and bring an end to human conflict.",
               "poster_path": "/gfJGlDaHuWimErCr5Ql0I8x9QSy.jpg",
               "release_date": "2017-05-30",
               "title": "Wonder Woman",
               "video": false,
               "vote_average": 7.3,
               "vote_count": 15742
            }
         ],
         "known_for_department": "Acting",
         "name": "Gal Gadot",
         "popularity": 51.346,
         "profile_path": "/9dlMlZiLtFr7v1yJ6Yy7Xp1jaXB.jpg"
      },
      {
         "gender": 1,
         "id": 4494,
         "known_for": [
            {
               "adult": false,
               "backdrop_path": "/vZh2ZoBMyAiH8vYGqadyMBX50Gc.jpg",
               "genre_ids": [
                  14,
                  35
               ],
               "id": 310,
               "media_type": "movie",
               "original_language": "en",
               "original_title": "Bruce Almighty",
               "overview": "Bruce Nolan toils as a 'human interest' television reporter in Buffalo, N.Y., but despite his high ratings and the love of his beautiful girlfriend, Bruce remains unfulfilled. At the end of the worst day in his life, he angrily ridicules God—and the Almighty responds, endowing Bruce with all of His divine powers.",
               "poster_path": "/nF9hly6Dk2pcQFSnuxugI34RpCC.jpg",
               "release_date": "2003-05-23",
               "title": "Bruce Almighty",
               "video": false,
               "vote_average": 6.7,
               "vote_count": 8103
            },
         ],
         "known_for_department": "Acting",
         "name": "Lisa Ann Walter",
         "popularity": 43.515,
         "profile_path": "/3xeMzT5zOC0UM9DgHne3oGZmf1R.jpg"
      }
   ]
};