import fetch from 'node-fetch';

export const getMovies = () => {
	return fetch(
		`https://api.themoviedb.org/3/discover/movie?api_key=${process.env.TMDB_KEY}&language=en-US&include_adult=false&page=1`
	)
	.then(res => res.json())
	.then(json => json.results);
};

export const getMovie = id => {
	return fetch(
		`https://api.themoviedb.org/3/movie/${id}?api_key=${process.env.TMDB_KEY}`
	).then(res => res.json());
};

export const getGenres = () => {
	return fetch(
		`https://api.themoviedb.org/3/genre/movie/list?api_key=${process.env.TMDB_KEY}&language=en-US`
	)
	.then(res => res.json())
	.then(json => json.genres);
};

export const getMovieReviews = id => {
	return fetch(
		`https://api.themoviedb.org/3/movie/${id}/reviews?api_key=${process.env.TMDB_KEY}`
	)
	.then(res => res.json())
	.then(json => json.results);
};

export const getUpcomingMovies = () => {
	return fetch(
		`https://api.themoviedb.org/3/movie/upcoming?api_key=${process.env.TMDB_KEY}&language=en-US&page=1`
	)
	.then(res => res.json())
	.then(json => json.results);
};

export const getPopularMovies = () => {
	return fetch(
		`https://api.themoviedb.org/3/movie/popular?api_key=${process.env.TMDB_KEY}&language=en-US&page=1`
	)
	.then(res => res.json())
	.then(json => json.results);
 };

//  export const getLatestMovies = () => {
// 	return fetch(
// 		`https://api.themoviedb.org/3/movie/latest?api_key=${process.env.TMDB_KEY}&language=en-US&page=1`
// 	)
// 	.then(res => res.json())
// 	.then(json => json.results);
//  };


// *********************  TV Endpoints   *****************************
export const getTvShows = () => {
	return fetch(
		`https://api.themoviedb.org/3/discover/tv?api_key=${process.env.TMDB_KEY}&language=en-US&include_adult=false&page=1`
	)
	.then(res => res.json())
	.then(json => json.results);
};

export const getShow = id => {
	return fetch(
		`https://api.themoviedb.org/3/tv/${id}?api_key=${process.env.TMDB_KEY}`
	).then(res => res.json());
};

//  *******************  People Endpoints   ******************************************
export const getPeople = () => {
	return fetch(
		`https://api.themoviedb.org/3/person/popular?api_key=${process.env.TMDB_KEY}&language=en-US&page=1`
	)
	.then(res => res.json())
	.then(json => json.results);
};

export const getPerson = id => {
	return fetch(
		`https://api.themoviedb.org/3/person/${id}?api_key=${process.env.TMDB_KEY}&language=en-US&page=1`
	).then(res => res.json());
};