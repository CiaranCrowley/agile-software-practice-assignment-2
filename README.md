# Assignment 2 - Agile Software Practice.

Name: Ciarán Crowley

Note:  Develop branch contains better testing code as well as authentication requirements throughout the api.  Not merged as herokuapp does not work with authentication

## Target Web API.

...... Document the Web API that is the target for this assignment's CI/CD pipeline. Include the API's endpoints and any other features relevant to the creation of a suitable pipeline, e.g.

+ Get /api/movies - returns an array of movie objects.
+ Get /api/movies/:id - returns detailed information on a specific movie.
+ Get /api/upcoming - returns an array of upcoming movie objects.
+ Get /api/popular - returns an array of popular movie objects.
+ Get /api/tv - returns an array of tv show objects.
+ Get /api/tv/:id - returns detailed information on a specific tv show.
+ Get /api/people - returns an array of tv show objects.
+ Get /api/people/:id - returns detailed information on a specific tv show.

## Error/Exception Testing.

.... From the list of endpoints above, specify those that have error/exceptional test cases in your test code, the relevant test file and the nature of the test case(s), e.g.

<h2>Movies Endpoint Tests</h2>

+ GET /api/movies - test that 20 movies are returned along with a status 200 
+ GET /api/movies/:id - test that a specified movie is returned with details on that movie
+ GET /api/movies/:id - test that no movie details are returned when the movie ID is invalid
+ GET /api/upcomng - tets that 20 upciming movies are returned with a status 200
+ GET /api/popular - test that 20 popular movies are returned with a status 200
+ POST /api/users/user1/favourites - test that Mulan is added as a favourite movie

<h2>TV Endpoint Tests</h2>

+ GET /api/tv - test that 20 tv shows are returned with a status 200
+ GET /api/tv/:id - test that a specific tv show is returned it details on that tv show
+ GET /api/tv/:id - test that no tv shows are returned when the id is invalid
+ POST /api/users/user1/favouriteShows - test that the Mandalorian is added as a favourite tv show

<h2>PEOPLE Endpoint Tests</h2>

+ GET /api/people - test that 20 actors are returned with a status 200
+ GET /api/people/:id - test that a specific actor is returned it details on that actor
+ GET /api/people/:id - test that no actors are returned when the id is invalid
+ POST /api/users/user1/favouriteActors - test that Gal Gadot is added as a Actor

<h2>USERS Endpoint Tests</h2>

+ GET /api/users - test that 2 actors are returned with a status 200
+ POST /apo/users - test that a 3rd user has been successfully added


## Continuous Delivery/Deployment.

..... Specify the URLs for the staging and production deployments of your web API, e.g.

+ https://movies-api-staging-ciaran.herokuapp.com - Staging deployment
+ https://movies-api-production-ciaran.herokuapp.com - Production


+ Staging app overview 

![stagingApp](/public/staging.png)

+ Production app overview 

![productionApp](/public/production.png)

## Feature Flags (If relevant)

... Specify the feature(s) in your web API that is/are controlled by a feature flag(s). Mention the source code files that contain the Optimizerly code that implement the flags. Show screenshots (with appropriate captions) from your Optimizely account that prove you successfully configured the flags.


[stagingapp]: ./img/stagingapp.png
